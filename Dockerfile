FROM python:2.7.15-alpine3.8

MAINTAINER Jon Baker <jonbaker85@gmail.com>

#Create directory
RUN mkdir /ztpserver
WORKDIR /ztpserver

#Add git
RUN apk add --update git

#Clone ZTP repository
RUN git clone -b master https://github.com/arista-eosplus/ztpserver.git /ztpserver \
   && pip install -r requirements.txt \
   && pip install gunicorn \
   && pip install futures

#Build and install ZTPServer
RUN python setup.py build \
    && python setup.py install

#Copy wsgi script for gunicorn
COPY ["./files/wsgi.py", "/etc/ztpserver/wsgi.py"]

#Copy gunicorn config script
COPY ["./files/gunicorn.conf", "./gunicorn.conf"]

# Set GUNICORN Env variables (All variables need to be prefixed with GUNICORN_)
ENV GUNICORN_TIMEOUT=120
ENV GUNICORN_WORKERS=5
ENV GUNICORN_CHDIR=/etc/ztpserver
ENV GUNICORN_BIND=0.0.0.0:8080
ENV GUNICORN_NAME=ztpserver
ENV GUNICORN_THREADS=2
#ENV GUNICORN_LOG-LEVEL=Debug

#Copy entrypoint script
COPY ["./files/entrypoint.sh", "/docker-entrypoint.sh"]

ENTRYPOINT ["sh", "/docker-entrypoint.sh"]
