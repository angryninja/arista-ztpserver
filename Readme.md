## Arista ZTPServer

This repository contains the following

- **Dockerfile** for building an [Arista's ZTPServer](https://ztpserver.readthedocs.io) on [Docker](https://www.docker.com/).
- **Docker-Compose** to build the additional services required
- Directory structure needed to pass through to the ztpserver, nginx and dhcp

### Base Docker Image

* [python:2.7.15-alpine3.8](https://hub.docker.com/_/python/)


### Installation

1. Install [Docker](https://www.docker.com/).

2. Download the image from the [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull angryninja/arista-ztpserver`

   (alternatively, you can build an image from the Dockerfile: `docker build -t="angryninja/arista-ztpserver" gitlab.com/angryninja/arista-ztpserver`)


### Usage

#### Standalone

    docker run -d angryninja/arista-ztpserver

#### Docker-compose

    docker-compose up -d

#### Configuring

The following files will need to be modified:

1. **dhcpd.conf** - Edit dhcp scope to suit your environment
2. **neighbordb** - See [neighbordb](https://ztpserver.readthedocs.io/en/master/config.html#static-provisioning-pattern)
3. **ztpserver.conf** - Update the server_url to suit your environment
4. **definitions** - See [definitions](https://ztpserver.readthedocs.io/en/master/config.html#static-provisioning-definition)
