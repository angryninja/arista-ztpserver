#!/bin/bash

#LOG_FOLDER="/var/logs"

if [ -n "$LOG_FOLDER" ]; then
    ACCESS_LOG=${LOG_FOLDER}/access.log
    ERROR_LOG=${LOG_FOLDER}/error.log
else
    ACCESS_LOG=-
    ERROR_LOG=-
fi

exec gunicorn \
    --config gunicorn.conf \
    --access-logfile "$ACCESS_LOG" \
    --error-logfile "$ERROR_LOG" \
     wsgi:app
"$@"
