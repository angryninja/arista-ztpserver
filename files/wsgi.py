import os.path
from ztpserver.app import start_wsgiapp

file_location = os.path.dirname(os.path.abspath(__file__))
confpath = os.path.join(file_location, 'ztpserver.conf')

app = start_wsgiapp(confpath)

