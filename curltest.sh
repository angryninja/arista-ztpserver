#!/bin/bash

function usage() {
echo "Usage:  $0 host count size port"
}

host=10.20.0.15
count=$1
port=8080

let i=$count-1
while [ $i -ge 0 ];
do
curl -w "$i: %{time_total} %{http_code} %{size_download} %{url_effective}\n" -o "/dev/null" -s http://${host}:${port}/files/images/vEOS-lab-4.21.2F.swi
let i=i-1
done

# To test in parallel:
# j2 = two parallel jobs
#parallel -n0 -j2 "time curl -w '{} %{time_total} %{http_code} %{size_download} %{url_effective}\n' -o '/dev/null' -s http://10.20.0.16:8080/files/images/vEOS-lab-4.21.2F.swi" ::: {1..2}
